import React from "react"
import { StyleSheet, Text, TouchableOpacity } from "react-native"

const CustomButton = ({ label, callback }) => {
    return (
        <TouchableOpacity
            style={styles.submit}
            underlayColor="#fff"
            onPress={() => callback()}
        >
            <Text style={styles.submitText}>{label}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    submit: {
        height: 60,
        marginTop: 10,
        paddingTop: 20,
        paddingBottom: 20,
        backgroundColor: "#79a6fe",
        borderRadius: 10,
        borderWidth: 1,
    },
    submitText: {
        color: "#fff",
        textAlign: "center",
        fontWeight: "bold",
    },
})

export default CustomButton
